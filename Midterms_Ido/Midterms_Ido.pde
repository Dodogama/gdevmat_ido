void setup()
{
  size(1080 , 720, P3D);
  camera (0 , 0 , -(height / 2.0f) / tan(PI * 30.0f / 180.0f),
    0, 0 ,0,
    0, -1, 0);
    background (0);
    for (int i = 0; i < 100; i++)
    {
      otherMatter[i] = new Mover();
    }
    blackHole.spawnHole();
    makeMatters();
}

Mover blackHole = new Mover();
Mover[] otherMatter = new Mover[100];

int timeCounter =  0;

void draw ()
{
    background(0);
    int resetTimer = 100; 
    timeCounter++;
    
    if(timeCounter > resetTimer)
    {
      timeCounter = 0;
      makeMatters();
      blackHole.spawnHole();
    }
    
    moveToBlackHole();
    blackHole.drawHole();
}

void makeMatters()
{
  for (int i = 0; i < 100; i++)
  {
     otherMatter[i].spawnMatters(); 
  }
}

void moveToBlackHole()
{
  for( int j = 0; j < 100; j++)
  {
    PVector moveIn = PVector.sub(blackHole.location , otherMatter[j].location);
    moveIn.normalize();
    moveIn.mult(15);
    otherMatter[j].location.add(moveIn);
    otherMatter[j].drawMatter();
  }  
}
