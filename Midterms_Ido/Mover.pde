public class Mover
{
   public PVector location;
   public float scale = 50;
   public float r = 255, g = 255, b = 255, a = 255;
   
    //BLACKHOLE
   Mover()
   {
      location = new PVector(); 
   }
   
   public void spawnHole ()
   {
      location.x = random(Window.left ,Window.right);
      location.y = random(Window.bottom , Window.top);
   }
      
   public void drawHole()
   {
      noStroke();
      fill(255);
      circle(location.x , location.y , 50);
   }
   
  // other matters
  
  //spawning a matter
  public void spawnMatters()
   {
    float gaussian = randomGaussian();
    float mean = 0;
    float standardDeviation = 240;
    float xMean =random(Window.left, Window.right);
    float yMean = random(Window.bottom , Window.top);
   
   location.x = (standardDeviation * gaussian) + xMean;
   location.y = (standardDeviation * gaussian) + yMean;
   }
   
   //Print Matter
   public void drawMatter()
   {
      noStroke();
      fill(random(255) , random(255) , random(255) , 100);
      circle(location.x , location.y , random(25));
   }
}
