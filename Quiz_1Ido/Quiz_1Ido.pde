void setup()
{
  size(1920 , 720, P3D);
  camera (0 , 0 , -(height / 2.0f) / tan(PI * 30.0f / 180.0f),
    0, 0 ,0,
    0, -1, 0);
    background (0);
}

void draw()
{
  float gaussian = randomGaussian();
  float mean = 0;
  float standardDeviation = 240;
  
  float x =(standardDeviation *gaussian) + mean;
  int y = floor(random(-720 , 720));
 
  mean = 10;
  standardDeviation = 100;
  
  float rSize =(standardDeviation *gaussian) + mean;
  smooth();
  noStroke();
  fill(random(255) , random(255) , random(255) , random(255));
  circle(x, y , rSize);
  
}
