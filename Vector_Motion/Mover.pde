public class Mover
{
  public PVector location = new PVector();
  public float scale = 50;
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
 
  
  public void render()
  {
    update();
    circle(location.x , location.y , scale);
  
  }
  
  private void update()
  {
    this.velocity.add(this.acceleration);
    
    this.location.add(this.velocity);
  
  this.velocity.limit(30);
  }
}
