void setup()
{
  size(1080 , 720, P3D);
  camera (0 , 0 , Window.eyeZ,0, 0, 0 ,0,
     -1, 0);
 background (0);
 mover = new Mover();
 mover.location.x = Window.left + 50;
 mover.acceleration = new PVector(0.1,0);
}

Mover mover;

void draw()
{
  noStroke();
 background (255);
 fill(255,0,0);
 mover.render();
 
 if(mover.location.x > Window.right)
 {
  mover.location.x = Window.left +50; 
 }
 
 if(mover.location.x > (Window.left +50) / 2 && mover.velocity.x > 0)
 {
   mover.acceleration.x = -0.1;
 }
 
 if (mover.velocity.x <= 0)
 {
   mover.acceleration.x = 0;
   mover.velocity.x = 0;
 }
 
}
